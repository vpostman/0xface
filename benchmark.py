import random
import numpy
import time
import sklearn
import sklearn.decomposition
import sklearn.svm
import sklearn.cluster
import matplotlib
import matplotlib.pyplot
from math import *
from PIL import Image, ImageFilter
import glob
import os.path
class neuron:
	def __init__(self):
		self.w = [0.1*random.random(), 0.1*random.random()]
		self.w2=self.w
		self.w0=self.w
		self.y=0.0
		self.y0=0.0
		self.x0=self.w
		self.n=1
		self.h=0.0
		self.dw2=[0.0,0.0]
	def health(self, x):
		return abs(self.w[0])+abs(self.w[1])
	def __call__(self, x):
		self.h=self.health(0.0)
		self.x0=x
		self.y=self.y+0.25*abs(x[0]*tanh(self.w2[0])+x[1]*tanh(self.w2[1]))*abs(1-abs(self.y))
		self.dw2[0]=0.25/cosh(0.25*self.y)*x[0]*(1-abs(self.w2[0]))
		self.w2[0]+=self.dw2[0]
		self.dw2[1]=0.25/cosh(0.25*self.y)*x[1]*(1-abs(self.w2[1]))
		self.n+=1
		self.w2[1]+=self.dw2[1]
		if(abs(self.y)>self.y0/32.0):
			self.w=self.w2
			self.y0=abs(self.y)
			self.y=0.0
		#	print tanh(self.y0)
			return tanh(abs(self.y0))
		else:
#			print self.y/10.0
			return tanh(abs(self.y))/2.0
	def bw(self, y,y0):
			m1=((y)-y0*self.w2[1])/self.w2[0]
			m2=self.y0
			z=[3.0*tanh(abs(y*m1)),3.0*tanh(abs(y*m2))]
#			print z
			return z
	def backprop(self,y):
			err=[self.x0[0]*self.y0*y,self.x0[1]*self.y0*y]
			self.w2[0]+=0.1*err[0]*(1.0-self.w2[1])
			self.w2[1]+=0.1*err[1]*(1.0-self.w2[1])
			return [0.2*tanh(err[0]+self.dw2[0])*self.w2[0],0.2*tanh(err[1]+self.dw2[1])*self.w2[1]]
class hunit:
	def __init__(self, v=0):
		self.k=neuron()
		self.k1=neuron()
		self.k2=neuron()
		self.z=0
		self.w=v
	def connect(self, x, yplus,  yminus):
		self.yplus=yplus
		self.yminus=yminus
		self.x=x
		return self
	def train(self, train=1):
			if 1:
				self.z=self.k([self.yplus.get(), self.x.get()])
				self.z2=self.k1([self.yminus.get(), self.x.get()])
				
				self.w=self.k2([self.z,self.z2])
				self.h=self.k2.health(numpy.zeros(2))+self.k.health(numpy.zeros(2))
			
			return abs(self.h)
	def bw(self):
			self.backprop()
			self.z=self.k2.bw(self.w,self.k2.y0)
			x=self.k1.bw(self.z[0],self.k2.y0)
			x2=self.k.bw(self.z[1],self.k2.y0)
			self.x.add(x[1]+x2[1])
			self.yplus.add(x[0])
			self.yminus.add(x2[0])
			self.h=self.k2.health(numpy.zeros(2))+self.k.health(numpy.zeros(2))
			return abs(self.h)
	def backprop(self):
			self.z=self.k2.backprop(self.w)
			self.z2=self.k1.backprop(self.z[0])
			self.z3=self.k.backprop(self.z[1])
			self.x.add(self.z3[1]+self.z2[1])
			self.yplus.add(self.z2[0])
			self.yminus.add(self.z3[0])
	def get(self):
		return self.w
	def get_final(self):
		return self.k2.y0+self.w
	def set(self, w):
		self.w=w
	def add(self, w):
		self.w=self.w+w

class vunit:
	def __init__(self, v=None):	
		self.v=v
	def set(self, a):
		self.v=a
	def add(self, a):
		self.v=self.v+a
	def get(self):
		return self.v
	def train(self):
		return 9.0;
class network:
	def __init__(self):
		self.v=[]

		self.h002=[]
		self.h001=[]
		self.h000=[]
		self.h00=[]
		self.h01=[]
		self.h02=[]

		self.h03=[]
		self.h0=[]
		self.h1=[]
		self.h2=[]	
		self.h3=[]
		self.h4=[]
		self.h5=[]
		self.h6=[]
		self.h7=[]
		self.h8=[]
		for i in xrange(4096):
			self.v.append(vunit(0))
			
			
		for i in xrange(2048):
			self.h002.append(hunit(0))
		for i in xrange(1024):
			self.h001.append(hunit(0))
		for i in xrange(512):
			self.h000.append(hunit(0))
		for i in xrange(256):
			self.h00.append(hunit(0))
		for i in xrange(128):
			self.h01.append(hunit(0))
		for i in xrange(64):
			self.h02.append(hunit(0))
		for i in xrange(32):
			self.h03.append(hunit(0))
		for i in xrange(16):
			self.h0.append(hunit(0))
		for i in xrange(8):
			self.h1.append(hunit(0))
		for i in xrange(4):
			self.h2.append(hunit(0))
		for i in xrange(64):
			self.h3.append(hunit(0))
		for i in xrange(32):
			self.h4.append(hunit(0))
		for i in xrange(16):
			self.h5.append(hunit(0))
		for i in xrange(8):
			self.h6.append(hunit(0))
		for i in xrange(4):
			self.h7.append(hunit(0))
		for i in xrange(2):
			self.h8.append(hunit(0))
		for i in xrange(2048):
			self.h002[i].connect(self.v[i], self.v[(i+64)%4096], self.v[(i+1)%4096])
		for i in xrange(1024):
			self.h001[i].connect(self.h002[i], self.h002[(i+32)%2048], self.h002[(i+1)%2048])

		for i in xrange(512):
			self.h000[i].connect(self.h001[i], self.h001[(i+32)%1024], self.h001[(i+1)%1024])

		for i in xrange(256):
			self.h00[i].connect(self.h000[i], self.h000[(i+16)%512], self.h000[(i+1)%512])

		for i in xrange(128):
			self.h01[i].connect(self.h00[i], self.h00[(i+16)%256], self.h00[(i+1)%256])
		for i in xrange(64):
			self.h02[i].connect(self.h01[i], self.h01[(i+8)%128], self.h01[(i+1)%128])
		for i in xrange(32):
			self.h03[i].connect(self.h02[i], self.h02[(i+8)%64], self.h02[(i+1)%64])
		for i in xrange(16):
			self.h0[i].connect(self.h03[i], self.h03[(i+4)%32], self.h03[(i+1)%32])
		for i in xrange(8):
			self.h1[i].connect(self.h0[i], self.h0[(i+4)%16], self.h0[(i+1)%16])
		for i in xrange(4):
			self.h2[i].connect(self.h1[i], self.h1[(i+2)%8], self.h1[(i+1)%8])
		for i in xrange(64):
			self.h3[i].connect(self.h2[i%4], self.h2[(i/2)%4], self.h2[(i+1)%4])
		for i in xrange(32):
			self.h4[i].connect(self.h3[i], self.h3[(i+8)%64], self.h3[(i+1)%64])
		for i in xrange(16):
			self.h5[i].connect(self.h4[i], self.h4[(i+4)%32], self.h4[(i+1)%32])
		for i in xrange(8):
			self.h6[i].connect(self.h5[i], self.h5[(i+2)%16], self.h5[(i+1)%16])
		for i in xrange(4):
			self.h7[i].connect(self.h6[i], self.h6[(i+2)%8], self.h6[(i+1)%8])
			
		for i in xrange(2):
			self.h8[i].connect(self.h7[i], self.h7[(i+1)%4], self.h7[(i+1)%4])
	def set(self, i, v):
		self.v[i].set(v)
	def train1(self, train=1):
		
			for i in xrange(2048):

				self.h002[i].train(train)
				
			for i in xrange(1024):

				self.h001[i].train(train)
				
		
			for i in xrange(512):

				self.h000[i].train(train)
				
			for i in xrange(256):

				self.h00[i].train(train)

			for i in xrange(128):

				self.h01[i].train(train)

			for i in xrange(64):

				self.h02[i].train(train)

			for i in xrange(32):

				self.h03[i].train(train)
			for i in xrange(16):
				
				self.h0[i].train(train)
			for i in xrange(8):
				
				self.h1[i].train(train)
			for i in xrange(4):

				self.h2[i].train(train)
				
			for i in xrange(64):

				self.h3[i].train(train)
			for i in xrange(32):

				self.h4[i].train(train)
			for i in xrange(16):

				self.h5[i].train(train)
			for i in xrange(8):
				self.h6[i].train(train)
			for i in xrange(4):
				self.h7[i].train(train)
					
			for i in xrange(2):

				self.h8[i].train(train)
	def get(self):
		w=[]
		for i in self.h7:
			w.append(i.get_final())
		return w
	def train2(self):

		for i in self.h8:
			
			i.add(i.get()*10.0)
			
		for i in self.h7:
			
			i.add(i.get()*2.0+random.random())
			
		for i in self.h6:
			
			i.add(i.get()*1.0+random.random())
			
		for i in self.h5:
			
			i.add(i.get()*0.5+random.random())

		for i in self.h4:
			i.set(0)

		for i in self.h3:
			i.set(0)

		for i in self.h2:
			i.set(0)

		for i in self.h1:
			i.set(0)

		for i in self.h0:
			i.set(0)

		for i in self.h03:
			i.set(0)

		for i in self.h02:
			i.set(0)

		for i in self.h01:
			i.set(0)

		for i in self.h00:
			i.set(0)
			
		for i in self.h000:
			i.set(0)

		for i in self.h001:
			i.set(0)
			
		for i in self.h002:
			i.set(0)
		for i in self.v:
			i.set(0)
			
			
		for i in self.h8:
			i.bw()
		for i in self.h7:
			i.bw()
		for i in self.h6:
			i.bw()
		for i in self.h5:
			i.bw()
		for i in self.h4:
			i.bw()
		for i in self.h3:
			i.bw()
		for i in self.h2:
			i.bw()
		for i in self.h1:
			i.bw()
		for i in self.h0:
			i.bw()
		for i in self.h03:
			i.bw()
		for i in self.h02:
			i.bw()
		for i in self.h01:
			i.bw()
		for i in self.h00:
			i.bw()
			
		for i in self.h000:
			i.bw()
			
		for i in self.h001:
			i.bw()
			
		for i in self.h002:
			i.bw()
			
def encode(n, x):
	for i in xrange(4096):
		n.set(i, x[i])
	n.train1(0)
	return n.get()
def train_to_stream(x):
	n=network()
	for t in xrange(200):
		for i in xrange(4096):
			n.set(i, x[i+t*4096])
		n.train1()
		if(t>12):
			n.train2()
			n.train1()
			n.train2()
			n.train1()
	return n
def main(threshovermean):
	im=Image.new('RGBA',(248,248), "black")
	a=[]
	arr=glob.glob("lfw3/*/*.pgm")
	
	arr0=[]
	arr1=[]
	arr2=[]
	arr3=[]
	leq=[]
	fp=open("test.txt")
	for line in fp:
		la=line.split()
		#print la
		if len(la)==3:
			s1="lfw3/"+la[0]+"/"+la[0]+("_%04d" % int(la[1])) + ".pgm"
			s2="lfw3/"+la[0]+"/"+la[0]+("_%04d" % int(la[2])) + ".pgm"
		#	print s1
		#	print s2
			for i in arr:
				if i==s1 or i==s2:
					arr.remove(i)
			arr2.append(s1)
			arr3.append(s2)
			leq.append(True)
		elif len(la)==4:
			s1="lfw3/"+la[0]+"/"+la[0]+("_%04d" % int(la[1])) + ".pgm"
			s2="lfw3/"+la[2]+"/"+la[2]+("_%04d" % int(la[3])) + ".pgm"
		#	print s1
		#	print s2
			for i in arr:
				if i==s1 or i==s2:
					arr.remove(i)
			arr2.append(s1)
			arr3.append(s2)
			leq.append(False)
	fp.close()
	fp=open("train.txt")
	
	leq_train0=[]
	leq_train1=[]
	labels=[]
	for line in fp:
		la=line.split()
		#print la
		if len(la)==3:
			s1="lfw3/"+la[0]+"/"+la[0]+("_%04d" % int(la[1])) + ".pgm"
			s2="lfw3/"+la[0]+"/"+la[0]+("_%04d" % int(la[2])) + ".pgm"
			if not (la[0] in labels):
				labels.append(la[0])
		#	print s1
		#	print s2
			arr0.append(s1)
			arr1.append(s2)
		elif len(la)==4:
			s1="lfw3/"+la[0]+"/"+la[0]+("_%04d" % int(la[1])) + ".pgm"
			s2="lfw3/"+la[2]+"/"+la[2]+("_%04d" % int(la[3])) + ".pgm"
			if not (la[0] in labels):
				labels.append(la[0])
			if not (la[2] in labels):
				labels.append(la[2])
		#	print s1
		#	print s2
			arr0.append(s1)
			arr1.append(s2)
	for i in xrange(0, 200):
		if(random.random()<0.5):
			g=[k/255.0 for k in Image.open(arr0[random.randint(0,len(arr0)-1)]).resize((64,64)).getdata()]
			a.extend(g)
		else:
			g=[k/255.0 for k in Image.open(arr1[random.randint(0,len(arr1)-1)]).resize((64,64)).getdata()]
			a.extend(g)
	q=[]
	n=train_to_stream(numpy.array(a))
	iter=float(len(arr2)) #don't truncate this it'll mess up the division!
	#cl=sklearn.cluster.AffinityPropagation()
	mse=0
	a=[]
	b=[]
	c=[]
	d=[]
	for i in xrange(int(iter)):
			a.extend([m/255.0 for m in Image.open(arr2[i]).resize((64,64)).getdata()])
			b.extend([m/255.0 for k in Image.open(arr3[i]).resize((64,64)).getdata()])
			c.append(encode(n,a))
			d.append(encode(n,b))
	n2=(numpy.array(c))
	s=n2.shape
	n3=(numpy.array(d))
	mean=0.0
	for i in xrange(int(iter)):
		mean+=numpy.sqrt(numpy.sum((n3[i]-n2[i])*(n3[i]-n2[i])))/iter
	s=n3.shape
	fpos=0
	fneg=0
	tneg=0
	tpos=0
	#m2=cl.fit_predict(n2)
	#m3=cl.fit_predict(n3)
	for i in xrange(int(iter)):
		found_match=numpy.sqrt(numpy.sum((n3[i]-n2[i])*(n3[i]-n2[i])))<threshovermean*mean
		#print [n2[i],n3[i]]
		is_match=leq[i]
		noerr=found_match==is_match
		if(noerr==False):
			if(found_match):
				# print "false positive %s %s"%(arr2[i],arr3[i])
				fpos+=1
			else:
				# print "false negative %s %s"%(arr2[i],arr3[i])
				fneg+=1
			mse+=1
		elif found_match:
			tpos+=1
		else:
			tneg+=1
	#print (1.0-mse/(iter))*100, " % accuracy on this trial"
	#print (fpos/(iter))*100, " % false positive on this trial"
	#print (fneg/(iter))*100, " % false negative on this trial"
	return fpos/float(fpos+tneg), tpos/float(tpos+fneg)
		
if __name__=="__main__":
	for i in xrange(1,100):
		fpos,tpos=main(0.5+1.0/float(i))
		print fpos, " ", tpos
	
